<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WelcomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::middleware('auth')->group(function () {
    Route::get('/home', function () {
        return view('backend.home');
    });

    Route::get('roles', [RoleController::class, 'index'])->name('roles.index');


    // Category
    Route::get('categories/export/', [CategoryController::class, 'export'])->name('categories.export');
    Route::get('categories/pdf/', [CategoryController::class, 'downloadPdf'])->name('categories.pdf');
    Route::get('/trashed-categories', [CategoryController::class, 'trash'])
        ->name('categories.trashed')->middleware('age_checker');
    Route::get('/trashed-categories/{category}/restore', [CategoryController::class, 'restore'])->name('categories.restore');
    Route::delete('/trashed-categories/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');
    Route::resource('categories', CategoryController::class);

    
    // User
    Route::get('users/export/', [UserController::class, 'export'])->name('users.export');
    Route::get('users/pdf/', [UserController::class, 'downloadPdf'])->name('users.pdf');
    Route::get('/trashed-users', [UserController::class, 'trash'])
        ->name('users.trashed')->middleware('age_checker');
    Route::get('/trashed-users/{user}/restore', [UserController::class, 'restore'])->name('users.restore');
    Route::delete('/trashed-users/{user}/delete', [UserController::class, 'delete'])->name('users.delete');
    Route::resource('users', UserController::class)->except(
        ['create', 'store']
    );



});
